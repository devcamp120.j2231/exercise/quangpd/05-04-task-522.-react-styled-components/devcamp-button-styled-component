import styled from "styled-components"

const PropsButton = styled.button`
    background-color : ${props => props.backColor};
    border-radius: 5px;
    color : ${props => props.fontColor};
    border : none;
    padding : 10px 20px;
    font-size : 14px;
    margin : 10px
`



export default PropsButton  

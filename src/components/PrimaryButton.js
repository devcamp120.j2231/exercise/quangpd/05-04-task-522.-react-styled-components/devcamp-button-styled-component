import styled from "styled-components"

const PrimaryButton = styled.button`
    background-color : ${props => props.primary ? "#7b4cd8" : "#ff31ca"};
    border-radius: 5px;
    color : ${props => props.primary ? "#e0d9f3" : "#ffb3e6"};
    border : none;
    padding : 10px 20px;
    font-size : 14px;
    margin : 10px
`

export default PrimaryButton
import styled from "styled-components";

// Cách 1
const StyledButton = styled.button`
    background-color : rgb(123,76,216);
    border-radius: 5px;
    color : white;
    border : none;
    margin-top : 10px;
    padding : 10px 20px;
    font-size : 14px;
`

//Cách 2 
// const StyledButton = styled['button']`
//     background-color : rgb(123,76,216);
//     border-radius: 5px;
//     color : white;
//     border : none;
//     margin-top : 10px;
//     padding : 10px 20px;
//     font-size : 14px;
// `

export default StyledButton

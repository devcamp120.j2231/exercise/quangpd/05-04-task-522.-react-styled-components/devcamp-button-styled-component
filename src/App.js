import StyledButton from './components/StyledButton';
import PropsButton from './components/PropsButton';
import PrimaryButton from './components/PrimaryButton';
import { SuperButton } from './components/ExtendedButton';
import { ExtendedButton } from './components/ExtendedButton';
import './App.css';

function App() {
  return (
    <>
      <div>
        <StyledButton>I am a styled button!</StyledButton>
      </div>
      <div>
        <PropsButton backColor ="#ffc3c3" fontColor ="#cfb76f">Button 1</PropsButton>
        <PropsButton backColor ="#ffdaca" fontColor ="#e07e64">Button 2</PropsButton>
        <PropsButton backColor ="#fff4c7" fontColor ="#">Button 3</PropsButton>
      </div>
      <div>
        <PrimaryButton primary>Primary Button  </PrimaryButton>
        <PrimaryButton > other button </PrimaryButton>
        <PrimaryButton > other button </PrimaryButton>
      </div>
      <div>
        <SuperButton>Supper Button</SuperButton>
        <ExtendedButton bgColor="#ff1993" ftColor="#ffe6f8">Hello 1!😍</ExtendedButton>
        <ExtendedButton bgColor="#ff69b4">Hello 2!</ExtendedButton>
        <ExtendedButton>Hello 3!</ExtendedButton>
      </div>
    </>
    
  );
}

export default App;
